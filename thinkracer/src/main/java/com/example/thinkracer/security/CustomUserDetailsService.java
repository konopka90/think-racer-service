package com.example.thinkracer.security;

import com.example.thinkracer.usermanagement.service.UserService;
import com.google.common.collect.Lists;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        var user = userService.getByEmail(email);
        return new User(user.getId(),
                user.getPassword(),
                user.isActivationCompleted(),
                true,
                true,
                true,
                Lists.newArrayList(
                        new SimpleGrantedAuthority("USER")
                )
        );
    }
}
