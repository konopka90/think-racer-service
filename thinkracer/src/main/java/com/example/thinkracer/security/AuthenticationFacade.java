package com.example.thinkracer.security;

import org.springframework.security.core.Authentication;

public interface AuthenticationFacade {
    /**
     * Common way for getting authentication from security context.
     * @return current authentication
     */
    Authentication getAuthentication();

    /**
     * Gets currently logged in User.
     * @return user ID.
     */
    String getUserId();
}
