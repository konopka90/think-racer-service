package com.example.thinkracer.usermanagement.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class User {
    @Id
    private String id;

    private String email;

    private String password;

    private String firstName;

    private String lastName;

    private boolean activationCompleted;

    private VerificationToken activationToken;

    private VerificationToken resetPasswordToken;
}
