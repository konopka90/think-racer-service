package com.example.thinkracer.usermanagement.service;

import com.example.thinkracer.usermanagement.dto.RegisterUserRequestDto;
import com.example.thinkracer.usermanagement.dto.ResendActivationEmailDto;
import com.example.thinkracer.usermanagement.dto.UpdateFullnameDto;
import com.example.thinkracer.usermanagement.dto.UpdatePasswordDto;
import com.example.thinkracer.usermanagement.model.User;

import java.util.Collection;

public interface UserService {
    /**
     * Lists all registered users.
     *
     * @return list of users.
     */
    Collection<User> list();

    /**
     * Get user by ID.
     *
     * @param id user ID
     * @return user with given ID
     * @throws com.example.thinkracer.usermanagement.exceptions.UserNotFoundException when user is not found
     */
    User getById(String id);

    /**
     * Get user by email address.
     *
     * @param email user email address
     * @return user with given email.
     * @throws com.example.thinkracer.usermanagement.exceptions.UserNotFoundException when user is not found
     */
    User getByEmail(String email);

    /**
     * Registers new user.
     *
     * @param registerUserRequestDto user details
     * @return newly created user.
     * @throws com.example.thinkracer.usermanagement.exceptions.EmailAlreadyRegisteredException when user exists in database with given email
     * @throws com.example.thinkracer.usermanagement.exceptions.EmailInvalidException           when email is invalid
     * @throws com.example.thinkracer.usermanagement.exceptions.PasswordInvalidException        when password is invalid
     */
    User register(RegisterUserRequestDto registerUserRequestDto);

    /**
     * Activates user with given token.
     *
     * @param token activation token
     * @throws com.example.thinkracer.usermanagement.exceptions.TokenInvalidException        when token not found in database
     * @throws com.example.thinkracer.usermanagement.exceptions.UserIsAlreadyActiveException when user is active
     */
    void activateUserWithToken(String token);

    /**
     * Resends activation email to the user.
     *
     * @param resendActivationEmailDto details about resending email.
     */
    void resendActivationEmail(ResendActivationEmailDto resendActivationEmailDto);

    /**
     * Sets user fullname.
     *
     * @param id                user ID
     * @param updateFullnameDto fullname details.
     * @throws com.example.thinkracer.usermanagement.exceptions.UserNotFoundException when user is not found.
     */
    void setFullname(String id, UpdateFullnameDto updateFullnameDto);

    /**
     * Updates user password.
     *
     * @param id                user ID
     * @param updatePasswordDto update password details.
     * @throws com.example.thinkracer.usermanagement.exceptions.UserNotFoundException    when user is not found
     * @throws com.example.thinkracer.usermanagement.exceptions.PasswordInvalidException when password does not meet conditions
     */
    void updatePassword(String id, UpdatePasswordDto updatePasswordDto);

    /**
     * Deletes user with given ID.
     * @param id user ID
     */
    void deleteUser(String id);
}