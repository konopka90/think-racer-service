package com.example.thinkracer.usermanagement.dto;

import com.example.thinkracer.usermanagement.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class UserDetailsDto {
    private String email;
    private String firstName;
    private String lastName;

    public static UserDetailsDto of(User user) {
        return UserDetailsDto
                .builder()
                .email(user.getEmail())
                .firstName(user.getFirstName())
                .lastName(user.getLastName())
                .build();
    }
}
