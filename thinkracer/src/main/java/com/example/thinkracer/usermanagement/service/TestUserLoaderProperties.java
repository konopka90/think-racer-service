package com.example.thinkracer.usermanagement.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "usermanagement.testuser")
@Getter
@Setter
public class TestUserLoaderProperties {
    String email;
    String password;
    boolean activationCompleted;
}
