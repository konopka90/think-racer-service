package com.example.thinkracer.usermanagement.service;

import com.example.thinkracer.usermanagement.model.VerificationToken;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.UUID;

@Service
public class TokenServiceImpl implements TokenService {

    @Override
    public VerificationToken createToken(int expireTokenInMinutes) {
        final VerificationToken verificationToken = new VerificationToken();
        verificationToken.setExpiryDate(calculateExpiryDate(expireTokenInMinutes));
        verificationToken.setToken(UUID.randomUUID().toString());
        return verificationToken;
    }

    @Override
    public boolean hasExpired(VerificationToken verificationToken) {
        final Calendar cal = Calendar.getInstance();
        return verificationToken.getExpiryDate().getTime() - cal.getTime().getTime() <= 0;
    }

    private Date calculateExpiryDate(int expireTokenInMinutes) {
        final Calendar cal = Calendar.getInstance();
        cal.setTime(new Timestamp(cal.getTime().getTime()));
        cal.add(Calendar.MINUTE, expireTokenInMinutes);
        return new Date(cal.getTime().getTime());
    }
}
