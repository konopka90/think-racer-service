package com.example.thinkracer.usermanagement.service;

import com.example.thinkracer.usermanagement.dto.RequestPasswordResetDto;
import com.example.thinkracer.usermanagement.dto.UpdatePasswordDto;
import com.example.thinkracer.usermanagement.model.User;

public interface ResetPasswordService {
    /**
     * Requests for password reset.
     * @param requestPasswordResetDto request about password reset.
     */
    void requestPasswordReset(RequestPasswordResetDto requestPasswordResetDto);

    /**
     * Validates reset password token.
     * @param token token to validate.
     * @throws com.example.thinkracer.usermanagement.exceptions.UserNotFoundException if user for given token not found
     * @throws com.example.thinkracer.usermanagement.exceptions.TokenInvalidException if token is invalid
     *
     * TODO: String is not best choice, maybe it would be better to pass VerificationToken
     */
    void validateResetPasswordToken(String token);

    /**
     * Updates user password. Throw exceptions like {@link UserService#updatePassword(String, UpdatePasswordDto)}
     * @param user user that needs password change
     * @param updatePasswordDto details about password update
     */
    void resetPassword(User user, UpdatePasswordDto updatePasswordDto);
}
