package com.example.thinkracer.usermanagement.controller;

import com.example.thinkracer.usermanagement.dto.RequestPasswordResetDto;
import com.example.thinkracer.usermanagement.dto.UpdatePasswordDto;
import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.service.ResetPasswordService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/reset-password")
public class ResetPasswordController {

    private final ResetPasswordService resetPasswordService;

    @PostMapping
    void requestPasswordReset(@RequestBody RequestPasswordResetDto requestPasswordResetDto) {
        resetPasswordService.requestPasswordReset(requestPasswordResetDto);
    }

    @GetMapping
    void validateToken(@RequestParam String token) {
        resetPasswordService.validateResetPasswordToken(token);
    }

    @PostMapping("/update")
    void updatePassword(@RequestBody UpdatePasswordDto updatePasswordDto) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        resetPasswordService.resetPassword(user, updatePasswordDto);
    }

}
