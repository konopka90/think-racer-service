package com.example.thinkracer.usermanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class EmailInvalidException extends RuntimeException {
    public EmailInvalidException() {
        super("Email is not valid");
    }
}
