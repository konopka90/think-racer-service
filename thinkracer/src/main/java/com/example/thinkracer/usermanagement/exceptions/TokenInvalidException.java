package com.example.thinkracer.usermanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class TokenInvalidException extends RuntimeException {
    public TokenInvalidException(String token) {
        super("Token " + token + " is invalid");
    }
}
