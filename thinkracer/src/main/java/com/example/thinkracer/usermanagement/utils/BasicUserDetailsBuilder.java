package com.example.thinkracer.usermanagement.utils;

import com.example.thinkracer.usermanagement.api.BasicUserDetailsMessage;
import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.service.TokenType;
import lombok.experimental.UtilityClass;

@UtilityClass
public class BasicUserDetailsBuilder {
    public static BasicUserDetailsMessage build(User user, TokenType tokenType) {
        return BasicUserDetailsMessage
                .builder()
                .email(user.getEmail())
                .userId(user.getId())
                .token(tokenType == TokenType.ACTIVATION ? user.getActivationToken().getToken() : user.getResetPasswordToken().getToken())
                .build();
    }
}
