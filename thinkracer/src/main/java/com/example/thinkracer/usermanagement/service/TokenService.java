package com.example.thinkracer.usermanagement.service;

import com.example.thinkracer.usermanagement.model.VerificationToken;

public interface TokenService {

    /**
     * Creates verification token.
     * @param expireTokenInMinutes time in minutes how long token is active.
     * @return new verification token
     */
    VerificationToken createToken(int expireTokenInMinutes);

    /**
     * Checks if token has expired.
     * @param verificationToken token to validate
     * @return true if token expired. Otherwise returns false.
     */
    boolean hasExpired(VerificationToken verificationToken);
}
