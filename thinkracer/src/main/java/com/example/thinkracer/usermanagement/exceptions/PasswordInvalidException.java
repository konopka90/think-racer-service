package com.example.thinkracer.usermanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class PasswordInvalidException extends RuntimeException {
    public PasswordInvalidException() {
        super("Password does not meet requirements");
    }
}
