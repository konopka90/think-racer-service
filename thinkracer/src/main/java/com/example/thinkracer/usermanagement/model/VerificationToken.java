package com.example.thinkracer.usermanagement.model;

import lombok.Data;

import java.util.Date;

@Data
public class VerificationToken {

    private String token;

    private Date expiryDate;
}
