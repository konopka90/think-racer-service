package com.example.thinkracer.usermanagement.api;

import lombok.experimental.UtilityClass;

/**
 * Definition of available channels for Spring Integration.
 */
@UtilityClass
public class Channels {
    public static final String REGISTERED_USER = "registeredUserChannel";

    public static final String RESEND_ACTIVATION_EMAIL = "resendActivationEmailChannel";

    public static final String REQUEST_RESET_PASSWORD = "requestResetPasswordChannel";
}
