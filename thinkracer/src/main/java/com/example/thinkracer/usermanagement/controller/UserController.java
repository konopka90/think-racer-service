package com.example.thinkracer.usermanagement.controller;


import com.example.thinkracer.security.AuthenticationFacade;
import com.example.thinkracer.usermanagement.dto.*;
import com.example.thinkracer.usermanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {

    private final UserService userService;

    private final AuthenticationFacade authenticationFacade;

    @PostMapping
    void registerNewUser(@RequestBody RegisterUserRequestDto registerUserRequestDto) {
        userService.register(registerUserRequestDto);
    }

    @GetMapping(value = "/activation")
    void activateUser(@RequestParam String token) {
        userService.activateUserWithToken(token);
    }

    @PostMapping(value = "/activation")
    void resendActivationEmail(@RequestBody ResendActivationEmailDto resendActivationEmailDto) {
        userService.resendActivationEmail(resendActivationEmailDto);
    }

    @PutMapping(value = "/fullname")
    void updateFullname(@RequestBody UpdateFullnameDto updateFullnameDto) {
        var userId = authenticationFacade.getUserId();
        userService.setFullname(userId, updateFullnameDto);
    }

    @PutMapping(value = "/password")
    void updatePassword(@RequestBody UpdatePasswordDto updatePasswordDto) {
        var userId = authenticationFacade.getUserId();
        userService.updatePassword(userId, updatePasswordDto);
    }

    @GetMapping(value = "/details")
    UserDetailsDto getUserDetails() {
        var userId = authenticationFacade.getUserId();
        return UserDetailsDto.of(userService.getById(userId));
    }
}
