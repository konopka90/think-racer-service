package com.example.thinkracer.usermanagement.model;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

public interface UserRepository extends MongoRepository<User, String> {
    Optional<User> findByEmail(String email);
    Optional<User> findByActivationTokenToken(String token);
    Optional<User> findByResetPasswordTokenToken(String token);

}
