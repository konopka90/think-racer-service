package com.example.thinkracer.usermanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserIsNotActiveException extends RuntimeException {
    public UserIsNotActiveException() {
        super("User is not active");
    }
}
