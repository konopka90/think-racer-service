package com.example.thinkracer.usermanagement.service;

public enum TokenType {
    ACTIVATION,
    PASSWORD_RESET
}
