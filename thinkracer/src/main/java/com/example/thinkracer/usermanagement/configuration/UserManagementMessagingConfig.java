package com.example.thinkracer.usermanagement.configuration;

import com.example.thinkracer.usermanagement.api.BasicUserDetailsMessage;
import com.example.thinkracer.usermanagement.api.Channels;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;
import org.springframework.integration.channel.PublishSubscribeChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.messaging.MessageChannel;

@EnableIntegration
@Configuration
public class UserManagementMessagingConfig {

    @Bean
    public MessageChannel registeredUserChannel() {
        return new PublishSubscribeChannel();
    }

    @Bean
    public MessageChannel resendActivationEmailChannel() {
        return new PublishSubscribeChannel();
    }

    @MessagingGateway
    public interface UserManagementGateway {
        @Gateway(requestChannel = Channels.REGISTERED_USER)
        void newUserRegisteredNotification(BasicUserDetailsMessage basicUserDetailsMessage);

        @Gateway(requestChannel = Channels.RESEND_ACTIVATION_EMAIL)
        void resendActivationEmailNotification(BasicUserDetailsMessage basicUserDetailsMessage);

        @Gateway(requestChannel = Channels.REQUEST_RESET_PASSWORD)
        void requestResetPasswordNotification(BasicUserDetailsMessage basicUserDetailsMessage);
    }
}
