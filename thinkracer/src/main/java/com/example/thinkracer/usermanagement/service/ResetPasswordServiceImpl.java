package com.example.thinkracer.usermanagement.service;

import com.example.thinkracer.usermanagement.configuration.UserManagementMessagingConfig;
import com.example.thinkracer.usermanagement.dto.RequestPasswordResetDto;
import com.example.thinkracer.usermanagement.dto.UpdatePasswordDto;
import com.example.thinkracer.usermanagement.exceptions.TokenInvalidException;
import com.example.thinkracer.usermanagement.exceptions.UserIsNotActiveException;
import com.example.thinkracer.usermanagement.exceptions.UserNotFoundException;
import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.model.UserRepository;
import com.example.thinkracer.usermanagement.utils.BasicUserDetailsBuilder;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;

@Service
@RequiredArgsConstructor
@Slf4j
public class ResetPasswordServiceImpl implements ResetPasswordService {

    private final UserRepository userRepository;

    private final UserService userService;

    private final TokenService tokenService;

    private final UserManagementMessagingConfig.UserManagementGateway userManagementGateway;

    private final PasswordEncoder passwordEncoder;

    @Value("${usermanagement.registration.expireTokenInMinutes}")
    private int expireResetPasswordTokenInMinutes;

    @Override
    public void requestPasswordReset(RequestPasswordResetDto requestPasswordResetDto) {
        User user = userService.getByEmail(requestPasswordResetDto.getEmail());

        if (!user.isActivationCompleted()) {
            log.error("User {} is not activated", requestPasswordResetDto.getEmail());
            throw new UserIsNotActiveException();
        }

        user.setResetPasswordToken(tokenService.createToken(expireResetPasswordTokenInMinutes));
        userRepository.save(user);

        sendNotificationResetPassword(user);

        log.info("User {} requested for password reset", user.getId());
    }

    @Override
    public void validateResetPasswordToken(String token) {
        var user = userRepository.findByResetPasswordTokenToken(token).orElseThrow(UserNotFoundException::new);

        if (tokenService.hasExpired(user.getResetPasswordToken())) {
            throw new TokenInvalidException(user.getResetPasswordToken().getToken());
        }

        setAuthentication(user);
        log.info("Reset password token validation completed for user {}", user.getId());
    }

    @Override
    public void resetPassword(User user, UpdatePasswordDto updatePasswordDto) {
        userService.updatePassword(user.getId(), updatePasswordDto);
        user = userService.getById(user.getId());
        user.setResetPasswordToken(null);
        userRepository.save(user);
        invalidateAuthentication();
        log.info("New password set for user {}", user.getId());
    }

    private void sendNotificationResetPassword(User user) {
        userManagementGateway.requestResetPasswordNotification(BasicUserDetailsBuilder.build(user, TokenType.PASSWORD_RESET));
    }

    private void setAuthentication(User user) {
        var auth = new UsernamePasswordAuthenticationToken(
                user,
                null,
                Collections.singletonList(new SimpleGrantedAuthority("CHANGE_PASSWORD_PRIVILEGE"))
        );
        SecurityContextHolder.getContext().setAuthentication(auth);
    }

    private void invalidateAuthentication() {
        SecurityContextHolder.getContext().setAuthentication(null);
    }

}
