package com.example.thinkracer.usermanagement.controller;

import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("/admin/user")
@RequiredArgsConstructor
public class AdminUserController {

    private final UserService userService;

    @GetMapping
    Collection<User> users() {
        return userService.list();
    }
}
