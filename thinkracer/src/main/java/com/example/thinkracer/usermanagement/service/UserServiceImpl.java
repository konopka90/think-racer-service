package com.example.thinkracer.usermanagement.service;

import com.example.thinkracer.usermanagement.configuration.UserManagementMessagingConfig;
import com.example.thinkracer.usermanagement.dto.RegisterUserRequestDto;
import com.example.thinkracer.usermanagement.dto.ResendActivationEmailDto;
import com.example.thinkracer.usermanagement.dto.UpdateFullnameDto;
import com.example.thinkracer.usermanagement.dto.UpdatePasswordDto;
import com.example.thinkracer.usermanagement.exceptions.*;
import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.model.UserRepository;
import com.example.thinkracer.usermanagement.utils.BasicUserDetailsBuilder;
import com.example.thinkracer.usermanagement.utils.EmailValidator;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;

@Service("userService")
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    private final UserManagementMessagingConfig.UserManagementGateway userManagementGateway;

    private final PasswordEncoder passwordEncoder;

    private final TokenService tokenService;

    @Value("${usermanagement.registration.expireTokenInMinutes}")
    private int expireRegistrationTokenInMinutes;

    @Profile("dev")
    @PostConstruct
    public void postConstruct() {
        userRepository.deleteAll();
        log.info("Clean up UserRepository database");
    }

    @Override
    public Collection<User> list() {
        return userRepository.findAll();
    }

    @Override
    public User getById(String id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User register(RegisterUserRequestDto registerUserRequestDto) {
        log.info("Request for user registration {}", registerUserRequestDto.toString());

        checkEmail(registerUserRequestDto.getEmail());
        checkPassword(registerUserRequestDto.getPassword());
        User user = createUser(registerUserRequestDto);
        sendNotificationUserRegistered(user);

        log.info("User registered {}", registerUserRequestDto.toString());
        log.info("User registered details {}", user.toString());

        return user;
    }

    @Override
    public void activateUserWithToken(String token) {
        log.info("User registration activation requested for token {}", token);

        var user = userRepository.findByActivationTokenToken(token);
        if (user.isEmpty()) {
            log.error("Token {} not found in database", token);
            throw new TokenInvalidException(token);
        }

        if (tokenService.hasExpired(user.get().getActivationToken())) {
            // TODO: remove account if token expired
            log.error("Token {} for user {} expired", token, user.get().getId());
            throw new TokenInvalidException(token);
        }

        User u = user.get();
        u.setActivationCompleted(true);
        u.setActivationToken(null);
        u = userRepository.save(u);

        log.info("User {} activated", u.getId());
    }

    @Override
    public void resendActivationEmail(ResendActivationEmailDto dto) {
        User user = getByEmail(dto.getEmail());

        if (user.isActivationCompleted()) {
            log.error("User {} is already active", user.getId());
            throw new UserIsAlreadyActiveException();
        }

        sendNotificationResendActivationEmail(user);
        log.info("User {} requested for resending activation email", user.getId());
    }

    @Override
    public void setFullname(String id, UpdateFullnameDto updateFullnameDto) {
        var user = getById(id);
        user.setFirstName(updateFullnameDto.getFirstName());
        user.setLastName(updateFullnameDto.getLastName());
        userRepository.save(user);
        log.info("User {} changed fullname", user.getId());
    }

    @Override
    public void updatePassword(String id, UpdatePasswordDto updatePasswordDto) {
        var user = getById(id);
        checkPassword(updatePasswordDto.getNewPassword());
        user.setPassword(passwordEncoder.encode(updatePasswordDto.getNewPassword()));
        log.info("User {} updated password", user.getId());
    }

    @Override
    public void deleteUser(String id) {
        userRepository.deleteById(id);
    }

    private void checkEmail(String email) {
        if (!EmailValidator.isValid(email)) {
            throw new EmailInvalidException();
        }

        var user = userRepository.findByEmail(email);
        user.ifPresent(u -> { throw new EmailAlreadyRegisteredException(u.getEmail()); });
    }

    private void checkPassword(String password) {
        if (password == null || password.isBlank()) {
            throw new PasswordInvalidException();
        }
    }

    private User createUser(RegisterUserRequestDto registerUserRequestDto) {
        User user = new User();
        user.setEmail(registerUserRequestDto.getEmail());
        user.setPassword(passwordEncoder.encode(registerUserRequestDto.getPassword()));
        user.setActivationToken(tokenService.createToken(expireRegistrationTokenInMinutes));
        return userRepository.save(user);
    }

    private void sendNotificationUserRegistered(User user) {
        userManagementGateway.newUserRegisteredNotification(BasicUserDetailsBuilder.build(user, TokenType.ACTIVATION));
    }

    private void sendNotificationResendActivationEmail(User user) {
        userManagementGateway.resendActivationEmailNotification(BasicUserDetailsBuilder.build(user, TokenType.ACTIVATION));
    }
}