package com.example.thinkracer.usermanagement.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class EmailValidator {
    public static boolean isValid(String email) {
        return org.apache.commons.validator.routines.EmailValidator.getInstance().isValid(email);
    }
}
