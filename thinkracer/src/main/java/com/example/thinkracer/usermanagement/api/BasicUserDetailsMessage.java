package com.example.thinkracer.usermanagement.api;


import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

/**
 * Message with basic user information sent between services via Spring Integration.
 */
@Data
@Builder
@RequiredArgsConstructor
public class BasicUserDetailsMessage {
    private final String userId;
    private final String email;
    private final String token;
}
