package com.example.thinkracer.usermanagement.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class UserIsAlreadyActiveException extends RuntimeException {
    public UserIsAlreadyActiveException() {
        super("User is already active");
    }
}
