package com.example.thinkracer.usermanagement.service;

import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.model.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.DependsOn;
import org.springframework.context.annotation.Profile;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
@DependsOn({"userService"})
@RequiredArgsConstructor
@Profile("dev")
@Slf4j
public class TestUserLoaderService {

    private final UserRepository userRepository;

    private final PasswordEncoder passwordEncoder;

    private final TestUserLoaderProperties testUserLoaderProperties;

    @PostConstruct
    public void postConstruct() {
        var user = userRepository.save(User.builder()
                .email(testUserLoaderProperties.getEmail())
                .password(passwordEncoder.encode(testUserLoaderProperties.getPassword()))
                .activationCompleted(testUserLoaderProperties.isActivationCompleted())
                .build());
        log.info("Test user loaded {}", user.toString());
    }
}
