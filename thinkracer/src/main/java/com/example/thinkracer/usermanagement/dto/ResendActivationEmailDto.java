package com.example.thinkracer.usermanagement.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class ResendActivationEmailDto {
    private String email;
}
