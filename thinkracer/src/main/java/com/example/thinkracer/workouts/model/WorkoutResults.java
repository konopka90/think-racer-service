package com.example.thinkracer.workouts.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
public class WorkoutResults {
    @Id
    private String id;

    private String userId;

    private String workoutId;

    private WorkoutLevel level;

    private Date creationDate;

    private int bestTime;

    private int avgTime;

    private int worstTime;

    private int totalTime;

    private int correctAnswers;

    private int wrongAnswers;

    private int totalAnswers;

    private float percent;
}
