package com.example.thinkracer.workouts.controller;

import com.example.thinkracer.security.AuthenticationFacade;
import com.example.thinkracer.workouts.dto.ClientWorkoutResultsDto;
import com.example.thinkracer.workouts.model.Workout;
import com.example.thinkracer.workouts.model.WorkoutResults;
import com.example.thinkracer.workouts.service.WorkoutLoaderService;
import com.example.thinkracer.workouts.service.WorkoutResultsGetDetails;
import com.example.thinkracer.workouts.service.WorkoutResultsService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Date;
import java.util.List;

@RequiredArgsConstructor
@RestController
@RequestMapping("/workouts")
public class WorkoutController {

    private final WorkoutLoaderService workoutLoaderService;

    private final WorkoutResultsService workoutResultsService;

    private final AuthenticationFacade authenticationFacade;

    @GetMapping
    public Collection<Workout> getAvailable() {
        return workoutLoaderService.list();
    }

    @GetMapping("/{workoutId}")
    public Workout getWorkout(@PathVariable String workoutId) {
        return workoutLoaderService.get(workoutId);
    }

    @GetMapping("/results/{workoutId}")
    public List<WorkoutResults> getWorkoutResults(@PathVariable String workoutId,
                                                  @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) Date from) {
        var userId = authenticationFacade.getUserId();
        var workout = workoutLoaderService.get(workoutId);
        return workoutResultsService.getResults(WorkoutResultsGetDetails
                .builder()
                .userId(userId)
                .workoutId(workoutId)
                .from(from)
                .build());
    }

    @PostMapping("/results")
    public void addResult(@RequestBody ClientWorkoutResultsDto dto) {
        var userId = authenticationFacade.getUserId();
        workoutResultsService.addResult(WorkoutResults
                        .builder()
                        .level(dto.getLevel())
                        .avgTime(dto.getAvgTime())
                        .bestTime(dto.getBestTime())
                        .totalTime(dto.getTotalTime())
                        .worstTime(dto.getWorstTime())
                        .correctAnswers(dto.getCorrectAnswers())
                        .wrongAnswers(dto.getWrongAnswers())
                        .totalAnswers(dto.getTotalAnswers())
                        .creationDate(dto.getDate())
                        .percent(dto.getPercent())
                        .workoutId(dto.getWorkoutId())
                        .userId(userId)
                        .build());
    }
}
