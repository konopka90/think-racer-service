package com.example.thinkracer.workouts.model;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface WorkoutResultsRepository extends MongoRepository<WorkoutResults, String> {
    List<WorkoutResults> findByUserIdAndWorkoutIdAndCreationDateGreaterThanEqual(String userId, String workoutId, Date date);
}
