package com.example.thinkracer.workouts.service;

import com.example.thinkracer.workouts.model.Workout;

import java.util.Collection;

public interface WorkoutLoaderService {

    /**
     * Get all available workouts.
     * @return collection of all workouts
     */
    Collection<Workout> list();

    /**
     * Get one specific workout by ID
     * @param id workout ID
     * @return if found returns Workout
     * @throws com.example.thinkracer.workouts.exceptions.WorkoutNotFoundException if workout not found
     */
    Workout get(String id);
}
