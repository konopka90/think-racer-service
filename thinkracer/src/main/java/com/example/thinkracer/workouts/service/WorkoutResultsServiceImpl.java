package com.example.thinkracer.workouts.service;

import com.example.thinkracer.workouts.model.WorkoutResults;
import com.example.thinkracer.workouts.model.WorkoutResultsRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class WorkoutResultsServiceImpl implements WorkoutResultsService {

    private final WorkoutResultsRepository resultsRepository;

    @Override
    public List<WorkoutResults> getResults(WorkoutResultsGetDetails details) {
        return resultsRepository.findByUserIdAndWorkoutIdAndCreationDateGreaterThanEqual(
                details.getUserId(), details.getWorkoutId(), details.getFrom());
    }

    @Override
    public WorkoutResults addResult(WorkoutResults workoutResults) {
        return resultsRepository.save(workoutResults);
    }

}
