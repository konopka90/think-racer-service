package com.example.thinkracer.workouts.service;

import lombok.AccessLevel;
import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.util.Date;


@RequiredArgsConstructor
@Builder
@Getter
@FieldDefaults(makeFinal = true, level = AccessLevel.PRIVATE)
public class WorkoutResultsGetDetails {
    String userId;
    String workoutId;
    Date from;
}
