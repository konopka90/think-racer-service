package com.example.thinkracer.workouts.service;

import com.example.thinkracer.workouts.model.Workout;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConfigurationProperties(prefix = "workouts.loader")
@Getter
@Setter
public class WorkoutLoaderProperties {
    private List<Workout> workouts;
}