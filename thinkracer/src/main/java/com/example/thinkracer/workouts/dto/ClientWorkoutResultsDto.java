package com.example.thinkracer.workouts.dto;

import com.example.thinkracer.workouts.model.WorkoutLevel;
import lombok.*;

import java.util.Date;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Builder
@Setter
public class ClientWorkoutResultsDto {
    private String workoutId;

    private WorkoutLevel level;

    private Date date;

    private int bestTime;

    private int avgTime;

    private int worstTime;

    private int totalTime;

    private int correctAnswers;

    private int wrongAnswers;

    private int totalAnswers;

    private float percent;

}
