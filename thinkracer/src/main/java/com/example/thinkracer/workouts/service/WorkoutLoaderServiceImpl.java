package com.example.thinkracer.workouts.service;

import com.example.thinkracer.workouts.exceptions.WorkoutNotFoundException;
import com.example.thinkracer.workouts.model.Workout;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class WorkoutLoaderServiceImpl implements WorkoutLoaderService {

    private final WorkoutLoaderProperties workoutLoaderProperties;

    private final Map<String, Workout> workouts = new HashMap<>();

    @PostConstruct
    public void create() {
        workoutLoaderProperties.getWorkouts().forEach(w -> workouts.put(w.getId(), w));
    }

    @Override
    public Collection<Workout> list() {
        return workoutLoaderProperties.getWorkouts();
    }

    @Override
    public Workout get(String id) {
        var workout = workouts.get(id);
        if (workout == null) {
            throw new WorkoutNotFoundException(id);
        };

        return workout;
    }
}
