package com.example.thinkracer.workouts.model;

import lombok.*;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class WorkoutLevel {
    private String name;
}
