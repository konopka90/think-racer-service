package com.example.thinkracer.workouts.service;

import com.example.thinkracer.workouts.model.WorkoutResults;
import org.bson.Document;
import org.springframework.data.mongodb.core.mapping.event.BeforeSaveCallback;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
public class WorkoutResultBeforeSaveCallback implements BeforeSaveCallback<WorkoutResults> {
    @Override
    public WorkoutResults onBeforeSave(WorkoutResults entity, Document document, String collection) {
        if (entity.getCreationDate() == null) {
            entity.setCreationDate(new Date());
        }
        return entity;
    }
}
