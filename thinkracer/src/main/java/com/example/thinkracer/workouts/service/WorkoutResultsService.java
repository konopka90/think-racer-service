package com.example.thinkracer.workouts.service;

import com.example.thinkracer.workouts.model.WorkoutResults;

import java.util.List;

public interface WorkoutResultsService {
    List<WorkoutResults> getResults(WorkoutResultsGetDetails details);
    WorkoutResults addResult(WorkoutResults workoutResults);
}
