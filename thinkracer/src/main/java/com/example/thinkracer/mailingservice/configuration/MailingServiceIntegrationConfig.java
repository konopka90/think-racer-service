package com.example.thinkracer.mailingservice.configuration;

import com.example.thinkracer.mailingservice.PasswordResetServiceActivator;
import com.example.thinkracer.mailingservice.RegisteredUserServiceActivator;
import com.example.thinkracer.usermanagement.api.Channels;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.dsl.IntegrationFlows;

@Configuration
@RequiredArgsConstructor
public class MailingServiceIntegrationConfig {

    private final RegisteredUserServiceActivator registeredUserServiceActivator;

    private final PasswordResetServiceActivator passwordResetServiceActivator;

    @Bean
    public IntegrationFlow registeredUserMessageFlow() {
        return IntegrationFlows.from(Channels.REGISTERED_USER).handle(this.registeredUserServiceActivator).get();
    }

    @Bean
    public IntegrationFlow resendActivationEmailMessageFlow() {
        return IntegrationFlows.from(Channels.RESEND_ACTIVATION_EMAIL).handle(this.registeredUserServiceActivator).get();
    }

    @Bean
    public IntegrationFlow sendResetPasswordMessageFlow() {
        return IntegrationFlows.from(Channels.REQUEST_RESET_PASSWORD).handle(this.passwordResetServiceActivator).get();
    }
}
