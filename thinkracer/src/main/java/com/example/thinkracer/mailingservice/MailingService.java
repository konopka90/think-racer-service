package com.example.thinkracer.mailingservice;

import com.example.thinkracer.usermanagement.api.BasicUserDetailsMessage;

public interface MailingService {

    /**
     * Sends email to user with activation link.
     * @param basicUserDetailsMessage details about user
     */
    void sendRegistrationConfirmation(BasicUserDetailsMessage basicUserDetailsMessage);

    /**
     * Sends email to user with password reset link.
     * @param basicUserDetailsMessage details about user
     */
    void sendResetPassword(BasicUserDetailsMessage basicUserDetailsMessage);
}
