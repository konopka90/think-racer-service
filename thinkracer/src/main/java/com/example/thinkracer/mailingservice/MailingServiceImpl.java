package com.example.thinkracer.mailingservice;


import com.example.thinkracer.usermanagement.api.BasicUserDetailsMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class MailingServiceImpl implements MailingService {

    private static final String MESSAGE_FROM = "ThinkRacer <konopka90@gmail.com>";
    private static final String REGISTRATION_MESSAGE_SUBJECT = "Welcome to ThinkRacer!";
    private static final String REGISTRATION_MESSAGE_BODY = "Hello %s! Your link is http://localhost:8080/user/activation?token=%s";

    private static final String RESET_PASSWORD_MESSAGE_SUBJECT = "ThinkRacer - Reset Password";
    private static final String RESET_PASSWORD_MESSAGE_BODY = "Hello %s! Your reset password link is http://localhost:8080/reset-password?token=%s";

    private final JavaMailSender javaMailSender;

    @Override
    @Async
    public void sendRegistrationConfirmation(BasicUserDetailsMessage basicUserDetailsMessage) {
        sendMessage(MESSAGE_FROM,
                basicUserDetailsMessage.getEmail(),
                REGISTRATION_MESSAGE_SUBJECT,
                String.format(REGISTRATION_MESSAGE_BODY, basicUserDetailsMessage.getEmail(), basicUserDetailsMessage.getToken()));
    }

    @Override
    @Async
    public void sendResetPassword(BasicUserDetailsMessage basicUserDetailsMessage) {
        sendMessage(MESSAGE_FROM,
                basicUserDetailsMessage.getEmail(),
                RESET_PASSWORD_MESSAGE_SUBJECT,
                String.format(RESET_PASSWORD_MESSAGE_BODY, basicUserDetailsMessage.getEmail(), basicUserDetailsMessage.getToken()));
    }

    private void sendMessage(String from, String to, String subject, String body) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setFrom(from);
        message.setTo(to);
        message.setSubject(subject);
        message.setText(body);
        javaMailSender.send(message);
    }


}
