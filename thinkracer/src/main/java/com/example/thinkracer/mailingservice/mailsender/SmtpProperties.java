package com.example.thinkracer.mailingservice.mailsender;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SmtpProperties {
    private Boolean auth;
    private Boolean tls;
}
