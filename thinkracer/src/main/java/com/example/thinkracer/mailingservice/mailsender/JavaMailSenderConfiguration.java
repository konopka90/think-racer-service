package com.example.thinkracer.mailingservice.mailsender;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import java.util.Properties;

@Configuration
@RequiredArgsConstructor
public class JavaMailSenderConfiguration {

    private final JavaMailSenderProperties javaMailSenderProperties;

    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(javaMailSenderProperties.getHost());
        mailSender.setPort(javaMailSenderProperties.getPort());

        mailSender.setUsername(javaMailSenderProperties.getUsername());
        mailSender.setPassword(javaMailSenderProperties.getPassword());

        Properties props = mailSender.getJavaMailProperties();
        props.put("mail.transport.protocol", javaMailSenderProperties.getProtocol());
        props.put("mail.smtp.auth", javaMailSenderProperties.getSmtp().getAuth().toString());
        props.put("mail.smtp.starttls.enable", javaMailSenderProperties.getSmtp().getTls().toString());
        props.put("mail.debug", Boolean.toString(javaMailSenderProperties.isDebug()));

        return mailSender;
    }
}
