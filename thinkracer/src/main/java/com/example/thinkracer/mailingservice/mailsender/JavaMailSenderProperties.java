package com.example.thinkracer.mailingservice.mailsender;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "mailing.mail")
@Getter
@Setter
public class JavaMailSenderProperties {

    private String host;
    private Integer port;
    private String username;
    private String password;
    private String protocol;
    private String from;
    private boolean debug;
    private SmtpProperties smtp;

}
