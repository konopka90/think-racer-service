package com.example.thinkracer.mailingservice;

import com.example.thinkracer.usermanagement.api.BasicUserDetailsMessage;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.ServiceActivator;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
@Slf4j
public class PasswordResetServiceActivator {

    private final MailingService mailingService;

    @ServiceActivator
    void onPasswordReset(BasicUserDetailsMessage message) {
        log.info("Service Activator message: {} ", message.toString());
        mailingService.sendResetPassword(message);
    }

}
