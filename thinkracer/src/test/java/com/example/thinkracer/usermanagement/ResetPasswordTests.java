package com.example.thinkracer.usermanagement;

import com.example.thinkracer.mailingservice.MailingService;
import com.example.thinkracer.usermanagement.api.BasicUserDetailsMessage;
import com.example.thinkracer.usermanagement.dto.RequestPasswordResetDto;
import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.model.UserRepository;
import com.example.thinkracer.usermanagement.service.ResetPasswordService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

import static org.mockito.Mockito.verify;

@SpringBootTest
@DirtiesContext
public class ResetPasswordTests {

    @Autowired
    private ResetPasswordService resetPasswordService;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private MailingService mailingService;

    private static User preloadedUser;

    @BeforeAll
    public static void initialize() {
        preloadedUser = User
                .builder()
                .email("preloaded@preloaded.com")
                .password("sample")
                .activationCompleted(true)
                .build();
    }

    @BeforeEach
    public void beforeEach() {
        userRepository.deleteAll();
    }

    @Test
    public void whenResetPasswordRequested_thenUserHasResetPasswordTokenAndSendEmail() {
        // given
        User user = userRepository.save(preloadedUser);

        // when
        resetPasswordService.requestPasswordReset(RequestPasswordResetDto
                .builder()
                .email(preloadedUser.getEmail())
                .build());

        // then user has reset password token
        user = userRepository.findById(user.getId()).orElseThrow();
        Assertions.assertNotNull(user.getResetPasswordToken());

        // then email is sent
        var argumentCaptor = ArgumentCaptor.forClass(BasicUserDetailsMessage.class);
        verify(mailingService).sendResetPassword(argumentCaptor.capture());
        Assertions.assertEquals(preloadedUser.getEmail(), argumentCaptor.getValue().getEmail());
    }
}
