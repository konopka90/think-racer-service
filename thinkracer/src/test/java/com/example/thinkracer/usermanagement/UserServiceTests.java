package com.example.thinkracer.usermanagement;

import com.example.thinkracer.mailingservice.MailingService;
import com.example.thinkracer.usermanagement.dto.RegisterUserRequestDto;
import com.example.thinkracer.usermanagement.dto.UpdateFullnameDto;
import com.example.thinkracer.usermanagement.exceptions.EmailAlreadyRegisteredException;
import com.example.thinkracer.usermanagement.exceptions.EmailInvalidException;
import com.example.thinkracer.usermanagement.exceptions.PasswordInvalidException;
import com.example.thinkracer.usermanagement.model.User;
import com.example.thinkracer.usermanagement.model.UserRepository;
import com.example.thinkracer.usermanagement.service.UserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.annotation.DirtiesContext;

@SpringBootTest
@DirtiesContext
public class UserServiceTests {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepository;

    @MockBean
    private MailingService mailingService;

    private static User preloadedUser;
    private static User preloadedUserTemplate;
    private static RegisterUserRequestDto userDto1;
    private static RegisterUserRequestDto userBadEmailDto1;
    private static RegisterUserRequestDto userBadEmailDto2;
    private static RegisterUserRequestDto userBadPasswordDto1;
    private static RegisterUserRequestDto userBadPasswordDto2;

    @BeforeAll
    public static void initialize() {
        preloadedUserTemplate = User
                .builder()
                .email("preloaded@preloaded.com")
                .password("sample")
                .build();


        userDto1 = RegisterUserRequestDto
                .builder()
                .email("test@test.com")
                .password("sample")
                .build();

        userBadEmailDto1 = RegisterUserRequestDto
                .builder()
                .email("a@")
                .password("sample")
                .build();

        userBadEmailDto2 = RegisterUserRequestDto
                .builder()
                .email("")
                .password("sample")
                .build();

        userBadPasswordDto1 = RegisterUserRequestDto
                .builder()
                .email("test2@test.com")
                .password("")
                .build();

        userBadPasswordDto2 = RegisterUserRequestDto
                .builder()
                .email("test2@test.com")
                .password(" ")
                .build();
    }

    @BeforeEach
    public void beforeEach() {
        userRepository.deleteAll();
        preloadedUser = userRepository.save(preloadedUserTemplate);
    }

    @Test
    public void whenRegisterNewUser_thenGetByIdOrGetByEmailAndSendEmail() {
        // when
        var user = userService.register(userDto1);

        // then
        Assertions.assertNotNull(user);
        Assertions.assertNotNull(user.getActivationToken());
        Assertions.assertEquals(userService.getById(user.getId()), user);
        Assertions.assertEquals(userService.getByEmail(user.getEmail()), user);


        Mockito.verify(mailingService, Mockito.times(1))
                .sendRegistrationConfirmation(Mockito.any());
    }

    @Test
    public void whenActivateUser_thenUserIsActivated() {
        // when
        var user = userService.register(userDto1);
        userService.activateUserWithToken(user.getActivationToken().getToken());

        // then
        user = userService.getById(user.getId());
        Assertions.assertTrue(user.isActivationCompleted());
    }

    @Test
    public void whenRegisterTwoTheSameUsers_thenThrowException() {
        // when
        var user = userService.register(userDto1);

        // then
        Assertions.assertNotNull(user);
        Assertions.assertThrows(EmailAlreadyRegisteredException.class, () -> userService.register(userDto1) );
    }

    @Test
    public void whenBadEmail_thenThrowException() {
        Assertions.assertThrows(EmailInvalidException.class, () -> userService.register(userBadEmailDto1));
        Assertions.assertThrows(EmailInvalidException.class, () -> userService.register(userBadEmailDto2));
    }

    @Test
    public void whenBadPassword_thenThrowException() {
        Assertions.assertThrows(PasswordInvalidException.class, () -> userService.register(userBadPasswordDto1));
        Assertions.assertThrows(PasswordInvalidException.class, () -> userService.register(userBadPasswordDto2));
    }

    @Test
    public void whenSetFullname_thenUserHasFullname() {
        // when
        userService.setFullname(preloadedUser.getId(), UpdateFullnameDto
                .builder()
                .firstName("A")
                .lastName("B")
                .build());

        // then
        var user = userService.getById(preloadedUser.getId());
        Assertions.assertEquals("A", user.getFirstName());
        Assertions.assertEquals("B", user.getLastName());
    }

    @Test
    public void whenDeleteUser_thenEmptyUserList() {
        // when
        userService.deleteUser(preloadedUser.getId());

        // then
        Assertions.assertEquals(0, userService.list().size());
    }
}