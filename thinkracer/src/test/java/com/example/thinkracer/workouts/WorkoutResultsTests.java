package com.example.thinkracer.workouts;

import com.example.thinkracer.workouts.model.Workout;
import com.example.thinkracer.workouts.model.WorkoutResults;
import com.example.thinkracer.workouts.model.WorkoutResultsRepository;
import com.example.thinkracer.workouts.service.WorkoutResultsGetDetails;
import com.example.thinkracer.workouts.service.WorkoutResultsService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.stream.Stream;

@SpringBootTest
@DirtiesContext
public class WorkoutResultsTests {

    @Autowired
    private WorkoutResultsService workoutResultsService;

    @Autowired
    private WorkoutResultsRepository workoutResultsRepository;

    static final String USER_ID = "test";
    static final String WORKOUT_ID = "test";
    static final String CREATED_DATE = "01/01/2001";
    static final SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

    static WorkoutResults workoutResults;

    @BeforeAll
    public static void initialize() throws ParseException {
        workoutResults = WorkoutResults
                .builder()
                .workoutId(WORKOUT_ID)
                .creationDate(formatter.parse(CREATED_DATE))
                .userId(USER_ID)
                .build();
    }

    @BeforeEach
    public void beforeEach() {
        workoutResultsRepository.deleteAll();
    }

    private static Stream<Arguments> provideDates() {
        return Stream.of(
                Arguments.of("01/01/2000", 1),
                Arguments.of("01/01/2001", 1),
                Arguments.of("01/01/2002", 0)
        );
    }

    @ParameterizedTest
    @MethodSource("provideDates")
    public void whenGetWorkoutResults_thenNumberOfResultsIsCorrect(String dateAsString, int expectedResults) throws ParseException {
        workoutResults = workoutResultsRepository.save(workoutResults);

        var date = formatter.parse(dateAsString);
        var results = workoutResultsService.getResults(
                WorkoutResultsGetDetails
                        .builder()
                        .userId(USER_ID)
                        .workoutId(WORKOUT_ID)
                        .from(date)
                        .build());
        Assertions.assertNotNull(results);
        Assertions.assertEquals(expectedResults, results.size());
    }

    @Test
    public void whenSaveWorkoutResults_thenResultsAreAddedCorrectly() {
        WorkoutResults results =  workoutResultsService.addResult(WorkoutResults.builder().workoutId("id").build());

        Assertions.assertNotNull(results);
        Assertions.assertNotNull(results.getCreationDate());

    }
}