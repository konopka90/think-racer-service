# ThinkRacer server

## Goal

The main goal of ThinkRacer server is to provide full backend functionality for two different projects: frontend application and desktop application.
So far application is not divided into separated micro-services because of very limited resources. Project will try to follow DDD principles and will be divided in the future.

## Simplified list of project requirements

- Authentication server 
	- Basic auth (stage 1)
	- Support for OAuth2 (stage 2)
	- Support for Facebook authentication
	- Support for Google authentication
- User Management
	- User registration
	- User activation
	- Password reset
	- Administration
- Workouts Service
	- Available workouts
	- Workout results storage
	- Workout results filtering
- Analytics Service
	- Workout reports based on workout results for users
	- Workout reports based on workout results for administrators
- Order Service
	- Order management
	- TBD...
- Payment Service
	- Paypal payment support
	- Credit card payment support
	- BLIK support
- Invoice Service
	- Invoice management
	- PDF exporter
- Mailing Service
	- Registration e-mails
	- Password reset e-mails
	- Advertising e-mails
	- Support e-mails
- Support Service
	- Provide technical support for users in case of problems with orders, payment, etc.
	
## Technology stack (stage 1)
- Spring + Spring Boot
- Spring Integration
- Spring Web
- Spring Contract Verifier (comming soon)
- Java Persistence API
- Docker
- MongoDB

## Development requirements
- Docker and Docker compose installed
- JDK 11

## Bring up infrastructure

Bring up MongoDB database

```
cd bootstrap/mongodb-docker && docker-compose up -d && cd -
```

## Build and run

```
cd thinkracer && ./gradlew run
```
